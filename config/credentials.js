'use strict';

const fs = require('fs');
const path = require('path');

const credentialsFolder = path.join(__dirname, '../.credentials/');
const cookiesFile = path.join(credentialsFolder, 'cookies.txt');
const cookiesContent = fs.readFileSync(cookiesFile);

module.exports = {
    'folder': credentialsFolder,
    'cookies': cookiesContent
};
