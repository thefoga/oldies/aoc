'use strict';

const solver = require('../../solver');

function solution(input) {
	const claimRegex = /#(\d+) @ (\d+),(\d+): (\d+)x(\d+)/;
	const grid = {};

	for (let i = 0; i < input.length; i++) {
		const claim = input[i];
		const matchGroups = claim.match(claimRegex);

		const offsetX = parseInt(matchGroups[2]);
		const offsetY = parseInt(matchGroups[3]);

		const width = parseInt(matchGroups[4]);
		const height = parseInt(matchGroups[5]);


		for (let j = 0; j < width; j++) {
			const x = offsetX + j;

			for (let k = 0; k < height; k++) {
				const y = offsetY + k;
				const previousValue = grid[`${x},${y}`];
				grid[`${x},${y}`] = previousValue === undefined ? 1 : previousValue + 1; // increase counter
			}
		}
	}

	const overlappedSpots =
		Object.values(grid)
			.filter(spot => spot >= 2);
	return overlappedSpots.length;
}

const result = solver.solve(2018, 3, solution);
console.log(result);
