'use strict';

const solver = require('../../solver');

function solution(input) {
	const claimRegex = /#(\d+) @ (\d+),(\d+): (\d+)x(\d+)/;
	const grid = {};
	const possibleUniques = new Set();

	for (let i = 0; i < input.length; i++) {
		const claim = input[i];
		const matchGroups = claim.match(claimRegex);

		const id = parseInt(matchGroups[1]);

		const offsetX = parseInt(matchGroups[2]);
		const offsetY = parseInt(matchGroups[3]);

		const width = parseInt(matchGroups[4]);
		const height = parseInt(matchGroups[5]);

		const overriddenIds = new Set();
		for (let j = 0; j < width; j++) {
			const x = offsetX + j;

			for (let k = 0; k < height; k++) {
				const y = offsetY + k;
				const previousValue = grid[`${x},${y}`];

				if (previousValue !== undefined) {
					overriddenIds.add(previousValue); // add id
				}

				grid[`${x},${y}`] = id; // mark as new id
			}
		}

		if (overriddenIds.size > 0) {
			for (let overriddenId of overriddenIds.values()) {
				possibleUniques.delete(overriddenId); // remove as it's overridden
			}
		} else {
			possibleUniques.add(id);
		}
	}

	return possibleUniques;
}

const result = solver.solve(2018, 3, solution);
console.log(result);
