<div align="center">
	<h1>Advent of Code</h1>
	<em>Solutions to 2018 challenges</em></br>

<a href="https://www.python.org/download/releases/3.6.0/"><img src="https://img.shields.io/badge/Python-3.6-blue.svg"></a>
<a href="https://nodejs.org/en/"><img src="https://img.shields.io/node/v/@stdlib/stdlib.svg"></a></br>

<a href="https://github.com/sirfoga/aoc/pulls"><img src="https://badges.frapsoft.com/os/v1/open-source.svg?v=103"></a> <a href="https://github.com/sirfoga/aoc/issues"><img src="https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat"></a> <a href="https://opensource.org/licenses/MIT"><img src="https://img.shields.io/badge/License-MIT-blue.svg"></a>

<a href="https://saythanks.io/to/sirfoga"><img src="https://img.shields.io/badge/Say%20Thanks-!-1EAEDB.svg" alt="Say Thanks!" /></a>
</div>


## Got questions?

If you have questions or general suggestions, don't hesitate to submit
a new [Github issue](https://github.com/sirfoga/aoc/issues/new).


## Contributing
[Fork](https://github.com/sirfoga/aoc/fork) | Patch | Push | [Pull request](https://github.com/sirfoga/aoc/pulls)


## Feedback
Suggestions and improvements are [welcome](https://github.com/sirfoga/aoc/issues)!


## Authors

| [![sirfoga](https://avatars0.githubusercontent.com/u/14162628?s=128&v=4)](https://github.com/sirfoga "Follow @sirfoga on Github") |
|---|
| [Stefano Fogarollo](https://sirfoga.github.io) |


## Thanks
Thanks to the creators of [Advent of Code](https://adventofcode.com/2018/), especially [Eric Wastl](https://github.com/topaz)


## License
[MIT License](https://opensource.org/licenses/MIT)
