'use strict';

const solver = require('../../solver');

function countLineOccurrences(line) {
	const chars = line.split('');
	const result = {};

	for (let i = 0; i < chars.length; i++) {
		if (!result[chars[i]]) {
			result[chars[i]] = 0;
		}

		result[chars[i]] += 1;
	}

	return result;
}

function countOccurrence(line, times) {
	const occurrences = countLineOccurrences(line);

	for (let key in occurrences) {
		const occurrence = occurrences[key];
		if (occurrence === times) {
			return 1;
		}
	}

	return 0;
}

function count2sAnd3s(line) {
	return {
		'2s': countOccurrence(line, 2),
		'3s': countOccurrence(line, 3)
	}
}

function solution(input) {
	let twoS = 0, threes0 = 0;

	for (let i = 0; i < input.length; i++) {
		const line = input[i];
		console.log(line);
		const occurrences = count2sAnd3s(line);
		console.log(occurrences);

		twoS += occurrences['2s'];
		threes0 += occurrences['3s'];
	}

	return twoS * threes0;
}

const result = solver.solve(2018, 2, solution);
console.log(result);
