'use strict';

const solver = require('../../solver');

function arrayDistance(arr0, arr1) {
	if (arr0.length > arr1.length) {
		return arrayDistance(arr1, arr0);
	}

	let counter = arr1.length - arr0.length; // arr1 is always longer than arr0
	for (let i = 0; i < arr0.length; i++) {
		if (arr0[i] !== arr1[i]) {
			counter += 1;
		}
	}

	return counter;
}

function stringDistance(string0, string1) {
	const chars0 = string0.split('');
	const chars1 = string1.split('');

	return arrayDistance(chars0, chars1);
}

function leastDifferent(strings) {
	let minDifference = strings[0].length;
	let leastDifferent = [null, null]; // placeholder values

	for (let i = 0; i < strings.length; i++) {
		for (let j = i + 1; j < strings.length; j++) {
			const diff = stringDistance(strings[i], strings[j]);
			if (diff < minDifference) {
				minDifference = diff;
				leastDifferent[0] = strings[i];
				leastDifferent[1] = strings[j];
			}
		}
	}

	return leastDifferent;
}

function commonChars(string0, string1) {
	let commons = [];
	for (let i = 0; i < string0.length; i++) {
		if (string0[i] === string1[i]) {
			commons.push(string0[i]);
		}
	}

	return commons.join(''); // to string
}

function solution(input) {
	const leastDifferentStrings = leastDifferent(input);
	return commonChars(
		leastDifferentStrings[0],
		leastDifferentStrings[1]
	);
}

const result = solver.solve(2018, 2, solution);
console.log(result);
