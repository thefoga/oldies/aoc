'use strict';

const solver = require('../../solver');

function solution(input) {
	const lines = input.map((el) => {
		return Number(el);
	});

	let i = 0;
	let currentValue = 0;
	const foundValues = new Set();
	while (true) {
		foundValues.add(currentValue);
		currentValue += lines[i];

		if (foundValues.has(currentValue)) {
			return currentValue;
		}

		foundValues.add(currentValue);

		i += 1;
		i %= lines.length;
	}
}

const result = solver.solve(2018, 1, solution);
console.log(result);
