'use strict';

const solver = require('../../solver');

function solution(input) {
	const lines = input.map((el) => {
		return Number(el);
	});
	const reducer = function (accumulator, currentValue) {
		const parsed = parseInt(currentValue);
		return accumulator + parsed;
	};

	return lines.reduce(reducer, 0);
}

const result = solver.solve(2018, 1, solution);
console.log(result);
