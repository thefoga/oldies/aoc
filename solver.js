'use strict';

const request = require('request');
const credentials = require('./config/credentials');
const cookies = credentials.cookies;

const headers = {
	'User-Agent': 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
	'Cookie': cookies,
	'Accept': '/',
	'Connection': 'keep-alive'
}; // headers for request

/**
 * Creates URL of challenge
 * @param year challenge year
 * @param day challenge day
 * @returns {string} URL
 */
function getUrlFromChallenge(year, day) {
	return `https://adventofcode.com/${year}/day/${day}/input`;
}

/**
 * Returns body of test challenge
 * @returns {string} body
 */
function test() {
	return "#1 @ 1,3: 4x4\n" +
		"#2 @ 3,1: 4x4\n" +
		"#3 @ 5,5: 2x2\n";
}

/**
 * Splits body into array of lines
 * @param body challenge input
 * @returns {Array} lines
 */
function parseBody(body) {
	return body
		.split(/\r?\n/)
		.filter(x => x.length > 1) // discard empty lines
		.map(x => x.trim()); // trim line
}

/**
 * Makes request for challenge and deploys algorithm to solve
 * @param year challenge year
 * @param day challenge day
 * @param solution algorithm to solve challenge
 * @param testing whether it's a test or a real challenge
 */
function solve(year, day, solution, testing) {
	const options = {
		url: getUrlFromChallenge(year, day),
		method: 'GET',
		headers: headers
	}; // options for request

	if (testing) {
		const body = test();
		const input = parseBody(body);
		return solution(input);
	}

	request(options, (err, res, body) => {
		if (err) {
			return console.log(err);
		}

		const input = parseBody(body);
		return solution(input);
	});
}

module.exports = {
	'solve': solve
};
